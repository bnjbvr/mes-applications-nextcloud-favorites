---
title: "Mon mini-site Framalibre"
order: 0
in_menu: true
---
Nextcloud est un nuage.

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages</p>
      <div>
        <a href="https://beta.framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article>

---

Sinon je fais aussi de la musique.

<iframe title="Hades OST - Good Riddance - Piano cover" width="560" height="315" src="https://aperi.tube/videos/embed/6c4b966b-0dea-4791-acfc-af806734e491" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe> 